# vim-tomorrow-night-theme
A repackaging of Chris Kempson's Tomorrow Themes for installation with vim-plug.

## Install
* Install using [vim-plug](https://github.com/junegunn/vim-plug) by adding to `init.vim` or `.vimrc`
`Plug 'https://gitlab.com/iscreaman23/vim-tomorrow-night-theme.git'`

* Set theme in `vim`
`:colorscheme Tomorrow-Night-Bright`

## Credit
This theme was shamelessly stolen from Chris Kempson's [Tomorrow-Theme](https://github.com/ChrisKempson/Tomorrow-Theme)

## License
Released under the [MIT License](https://github.com/chriskempson/tomorrow-theme/blob/master/LICENSE.md)
